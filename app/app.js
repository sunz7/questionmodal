'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', []
)
.directive('questionModal', function(){
      return{
          restrict: 'E',
          scope: {
            types: '='
          },
          templateUrl: 'questionModal.tpl.html',
          link: function(scope, element, attrs, ctrls){

          }
      }
    })

.controller('modalCtrl', function($scope){

        $scope.options = [
            {name: 'Checkboxes', id: 0, options:['placeholder1', 'placeholder2']},
            {name: 'Dropdown', id: 1, options:[]}];
    });
